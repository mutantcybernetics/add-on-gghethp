# Add-on-GGHETHP

# GPS + Radio + WiFi + Bluetooth + Pressure + Humidity + Temperature + Gyro

## Compatible with mutantC v2/v3/v4(with modification)
Can able to turn off each module separately using Pi GPIO.

It has this module support (Click the blue color name to show the module)
- [Ubox-GPS](https://www.ebay.com/sch/i.html?_nkw=gyneo6mv2) (with PCB)
- [HC-12](https://www.ebay.com/sch/i.html?_nkw=HC-12)
- [ESP-32](https://www.ebay.com/sch/i.html?_nkw=ESP-WROOM-32+module)
- [BME280](https://www.ebay.com/sch/i.html?_nkw=bme280)
- [Gyro-MPU6050](https://www.ebay.com/sch/i.html?_nkw=mpu6050)


<img src="pic_real.png" width="500">
<img src="pic_board.png" width="500">
<img src="position.png" width="500">
<img src="show_off.png" width="500">
